//
//  AppDelegate.h
//  MapDemo
//
//  Created by Gavin on 2014-11-17.
//  Copyright (c) 2014 Gavin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWRevealViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SWRevealViewController *viewController;

@end

