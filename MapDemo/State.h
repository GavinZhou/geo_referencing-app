//
//  State.h
//  MapDemo
//
//  Created by Gavin on 2015-02-05.
//  Copyright (c) 2015 Gavin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "shapefil.h"
#import <MapKit/MapKit.h>

@interface State : NSObject

@property (nonatomic, strong) NSArray *polygons;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, assign) double minLat;
@property (nonatomic, assign) double maxLat;
@property (nonatomic, assign) double minLong;
@property (nonatomic, assign) double maxLong;


- (id)initWithShapeObject:(SHPObject *)shape;

@end

@interface Polygon : NSObject

@property (nonatomic, strong) NSArray *coordinates;

@end