//
//  ViewController.h
//  MapDemo
//
//  Created by Gavin on 2014-11-17.
//  Copyright (c) 2014 Gavin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>{
    
    MKPolyline* _routeLine;
    MKPolylineView* _routeLineView;
    
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *states;
@property (nonatomic, strong) NSMutableArray *canRoads;

@end

