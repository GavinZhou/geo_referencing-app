//
//  ViewController.m
//  MapDemo
//
//  Created by Gavin on 2014-11-17.
//  Copyright (c) 2014 Gavin. All rights reserved.
//

#import "ViewController.h"
#import "shapefil.h"
#import "State.h"
#import <mach/mach.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeStandard;
    
    // Create location manager
    self.locationManager = [[CLLocationManager alloc] init];
    
    // Set a delegate to receieve location callbacks
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    
    // Start location manager
    [self.locationManager startUpdatingLocation];
    
    self.mapView.showsUserLocation = YES;
    
    [self createButtons];

}

// Generate six buttons
- (void)createButtons
{
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button1 addTarget:self action:@selector(readFirstThousandData) forControlEvents:UIControlEventTouchUpInside];
    [button1 setTitle:@"1000 Roads" forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor blackColor];
    button1.frame = CGRectMake(0.0, 400.0, 100.0, 40.0);
    [self.mapView addSubview:button1];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button2 addTarget:self action:@selector(readSecondThousandData) forControlEvents:UIControlEventTouchUpInside];
    [button2 setTitle:@"2000 Roads" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor blackColor];
    button2.frame = CGRectMake(120.0, 400.0, 100.0, 40.0);
    [self.mapView addSubview:button2];
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button3 addTarget:self action:@selector(readThirdThousandData) forControlEvents:UIControlEventTouchUpInside];
    [button3 setTitle:@"3000 Roads" forState:UIControlStateNormal];
    button3.backgroundColor = [UIColor blackColor];
    button3.frame = CGRectMake(240.0, 400.0, 100.0, 40.0);
    [self.mapView addSubview:button3];
    
    UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button4 addTarget:self action:@selector(readFiveThousandData) forControlEvents:UIControlEventTouchUpInside];
    [button4 setTitle:@"5000 Roads" forState:UIControlStateNormal];
    button4.backgroundColor = [UIColor blackColor];
    button4.frame = CGRectMake(0.0, 500.0, 100.0, 40.0);
    [self.mapView addSubview:button4];

    
    UIButton *button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button5 addTarget:self action:@selector(readSevenThousandData) forControlEvents:UIControlEventTouchUpInside];
    [button5 setTitle:@"7000 Roads" forState:UIControlStateNormal];
    button5.backgroundColor = [UIColor blackColor];
    button5.frame = CGRectMake(120.0, 500.0, 100.0, 40.0);
    [self.mapView addSubview:button5];
    
    UIButton *button6 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button6 addTarget:self action:@selector(readTenThousandData) forControlEvents:UIControlEventTouchUpInside];
    [button6 setTitle:@"10000 Roads" forState:UIControlStateNormal];
    button6.backgroundColor = [UIColor blackColor];
    button6.frame = CGRectMake(240.0, 500.0, 100.0, 40.0);
    [self.mapView addSubview:button6];
}


- (void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 1600, 1600);
    // Uncomment following line to zoom in to current user location
//    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:NO];
}


// Overlay method, Display routes to the mapview
- (MKOverlayView*) mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
    
    MKOverlayView* overlayView = nil;
    
    if (overlay == _routeLine) {
        _routeLineView = [[MKPolylineView alloc] initWithPolyline:self->_routeLine];
        _routeLineView.fillColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957  alpha:1];
        _routeLineView.strokeColor = [UIColor colorWithRed:0.945 green:0.027 blue:0.957 alpha:1];
        _routeLineView.lineWidth = 4;
        
        overlayView = _routeLineView;
        return overlayView;
    }
    
    else if ([overlay isKindOfClass:[MKPolygon class]]){
        MKPolygonView *view = [[MKPolygonView alloc] initWithPolygon:overlay];
        view.strokeColor = [UIColor blueColor];
        view.lineWidth = 2;
        view.lineCap = kCGLineCapRound;
        
        return view;
    }
    else{
        return nil;
    }
}

// Read data from shapefile
- (void) readRoadsData:(int) numOfRoads
{
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString *shpPath = [resourcePath stringByAppendingPathComponent:@"CAN_roads"];
    
    const char * pszPath = [shpPath cStringUsingEncoding:NSUTF8StringEncoding];
    SHPHandle shp = SHPOpen(pszPath, "rb");
    
    int numEntities;
    int shapeType;
    SHPGetInfo(shp, &numEntities, &shapeType, NULL, NULL);
    NSLog(@"%d entities",numEntities);
    
    
    self.canRoads = [NSMutableArray arrayWithCapacity:numEntities];
    for (int i = 0 ; i < numOfRoads; i++) {
        SHPObject *shpObject = SHPReadObject(shp, i);
        State *state = [[State alloc] initWithShapeObject:shpObject];
        [self.canRoads addObject:state];
    }
    
    SHPClose(shp);
    [self drawRoads];
}

// helper method
- (NSString *) descriptionForFieldType:(DBFFieldType) fieldType
{
    switch (fieldType) {
        case FTDouble:
            return @"double";
            
        case FTInteger:
            return @"integer";
        case FTLogical:
            return @"logical";
        case FTString:
            return @"string";
        case FTInvalid:
            return @"<invalid>";
        default:
            return @"<unknown>";
    }
}

// Draw roads through overlay
-(void)drawRoads
{
    for (State *state in self.canRoads) {
        
        for (Polygon *polygon in state.polygons) {
            int count = [polygon.coordinates count];
            CLLocationCoordinate2D coords [count];
            
            for (int c = 0; c<count; c++) {
                NSValue *coordValue = polygon.coordinates[c];
                CLLocationCoordinate2D coord = [coordValue MKCoordinateValue];
                coords[c] = coord;
            }
            
            MKPolygon *polygon = [MKPolygon polygonWithCoordinates:coords count:count];
            [self.mapView addOverlay:polygon];
        }
    }

}

-(void) readFirstThousandData
{
    [self readRoadsData:1000];
}

-(void) readSecondThousandData
{
    [self readRoadsData:2000];
}

-(void) readThirdThousandData
{
    [self readRoadsData:3000];
}

-(void) readFiveThousandData
{
    [self readRoadsData:5000];
}

-(void) readSevenThousandData
{
    [self readRoadsData:7000];
}

-(void) readTenThousandData
{
    [self readRoadsData:10000];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
